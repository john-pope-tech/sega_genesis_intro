# Making a game from scratch

[WORK IN PROGRESS](https://steamcommunity.com/sharedfiles/filedetails/?id=1438796702)

[Changelog](https://steamcommunity.com/sharedfiles/filedetails/changelog/1438796702)

Following guide from: https://github.com/Stephane-D/SGDK/wiki/Setup-SGDK-basic

## Manual
Compile with:

	%GDK_WIN%\bin\make -f makefile.gen

Place `*.c` and `*.h` files in project root, `src/`, or `src/games/*/` folders (must be in a subfodler of games).

	#TODO: make the directory builds dynamic from the ${OBJ} variable
	#until then, update/add MKDIR paths around line 95
	pre-build:
		...
		$(MKDIR) -p out/src/game/inc
		$(MKDIR) -p out/src/game/models
		...

If you want to organize files differently, update makefile.gen file to match file structure (specify source locations around line 29, output directories around line 95).

## VS Code 2017

TODO: normalize thsese:

	# windows
	cp .vscode/tasks.json.windows .vscode/tasks.json
	# linux
	cp .vscode/tasks.json.linux .vscode/tasks.json

In the `.vscode/tasks.json` file, there are clean and build tasks defined (based on [pleft/SEGA_VSCode_Template](https://github.com/pleft/SEGA_VSCode_Template))

Use `ctrl + shift + B` to build

Add to your keybindings.json

    { "key": "ctrl+alt+shift+b",          "command": "workbench.action.tasks.runTask", "args": "clean/make" },

Use `ctrl + alt + shift + B` to do a clean and build

## Playing/Testing

### Option 1: Emulator

This is the preferred method and is rather quick for testing/dev.

Download an emulator such as [Kega Fusion](https://segaretro.org/Kega_Fusion)

`ctrl + G` to select and load the compiled `out/rom.bin` Repeat after each compile, or use `ctrl + alt + L` (may only work if using only one ROM with emulator as it loads from the file history)

### Option 2: Steam Workshop

This is an easy way to share with a friend. It does require owning at least one game (usually can find something for $1-5). Upload via ModManager in from steam tools (you must pick game to attach the mod to, most any you own should work, some may support settings like save games and others won't).

Once uploaded to steam, click on your profile -> content -> find your mod then subscribe. Any time you make changes, reupload to steam and wait for the update to be downloaded. To load the mod, select the game it was attached to -> mods -> select your mod. **Workshop items will only sync with the emulator closed.**

Specify a name, a game volume (game to apply the mod to) and upload your rom file. The file to upload is the generated rom file in `out/rom.bin`, all other settings are optional (you may want to make it private while you work on your game).

## Resources

* https://megacatstudios.com/blogs/press/sega-genesis-mega-drive-vdp-graphics-guide-v1-2a-03-14-17
* https://opengameart.org/content/wyrmsun-cc0-over-900-items
* https://overiq.com/c-programming/101/pointers-as-structure-member-in-c/
* https://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_palettes#9-bit_RGB

Software for graphics

* Aesprite ($15, humble bundle - good tool for this)
* https://github.com/ComputerNerd/Retro-Graphics-Toolkit/wiki (untested)
* https://www.irfanview.com/ (untested)