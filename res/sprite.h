#ifndef _RES_SPRITE_H_
#define _RES_SPRITE_H_

extern const SpriteDefinition skeleton_sprite;
extern const SpriteDefinition creep_sprite;
extern const SpriteDefinition bullet_sprite;
extern const SpriteDefinition fireball_sprite;
extern const SpriteDefinition selection_sprite;
extern const SpriteDefinition radius_sprite;

#endif // _RES_SPRITE_H_
