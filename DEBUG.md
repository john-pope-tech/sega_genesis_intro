# VS Code Notes (windows)

[Reference Guide](https://gendev.spritesmind.net/page-gdb.html)

Build SGDK with `debug` flag set:

    %GDK_WIN%\bin\make -f %GDK_WIN%\makelib.gen clean
    %GDK_WIN%\bin\make -f %GDK_WIN%\makelib.gen debug

Build project with `debug` flag set:

    %GDK_WIN%\bin\make -f makefile.gen clean
    %GDK_WIN%\bin\make -f makefile.gen debug

Attach debugger, use gensKmod to run `out.rom` file

## Sample VS Code configs

Add something like these to your `.vscode/tasks.json` file (assumes local makefiles, prefix `${env:GDK}\\` to `makefile.gen` and `makelib.gen` if using the SGDK makefiles)

    tasks: [
        {
            "isBuildCommand": true,
            "suppressTaskName": true,
            "echoCommand": true,
            "taskName": "make",
            "args": [
                "${env:GDK}\\bin\\make", "-f", "makefile.gen"
            ]

        },
        {
            "suppressTaskName": true,
            "echoCommand": true,
            "taskName": "clean/make",
            "args": [
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "clean", "&&",
                "${env:GDK}\\bin\\make", "-f", "makefile.gen"
            ]

        },
        {
            "suppressTaskName": true,
            "echoCommand": true,
            "taskName": "clean/make SGDK",
            "args": [
                "${env:GDK}\\bin\\make", "-f", "makelib.gen", "clean", "&&",
                "${env:GDK}\\bin\\make", "-f", "makelib.gen", "&&",
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "clean", "&&",
                "${env:GDK}\\bin\\make", "-f", "makefile.gen"
            ]
        },
        {
            "isBuildCommand": true,
            "suppressTaskName": true,
            "echoCommand": true,
            "taskName": "make (debug)",
            "args": [
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "debug"
            ]
        },
        {
            "suppressTaskName": true,
            "echoCommand": true,
            "taskName": "clean/make (debug)",
            "args": [
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "clean", "&&",
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "debug"
            ]
        },
        {
            "suppressTaskName": true,
            "echoCommand": true,
            "taskName": "clean/make SGDK (debug)",
            "args": [
                "${env:GDK}\\bin\\make", "-f", "makelib.gen", "clean", "&&",
                "${env:GDK}\\bin\\make", "-f", "makelib.gen", "debug", "&&",
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "clean", "&&",
                "${env:GDK}\\bin\\make", "-f", "makefile.gen", "debug"
            ]
        }
    ]

Add to `launch.json`

    {
        // Use IntelliSense to learn about possible attributes.
        // Hover to view descriptions of existing attributes.
        // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
        "version": "0.2.0",
        "configurations": [
            {
                "name": "(gdb) Launch",
                "type": "cppdbg",
                "request": "launch",
                "program": "${workspaceFolder}/out/rom.out",
                "args": [],
                "stopAtEntry": false,
                "cwd": "${workspaceFolder}",
                "environment": [],
                "externalConsole": true,
                "MIMode": "gdb",
                "miDebuggerServerAddress": "localhost:6868",
                "miDebuggerPath": "${env:GDK}\\bin\\gdb.exe",
                "setupCommands": [
                    {
                        "description": "Enable pretty-printing for gdb",
                        "text": "-enable-pretty-printing",
                        "ignoreFailures": true
                    }
                ]
            }
        ]
    }

Keyboard shortcuts

* `ctrl+shift+b` => `make`
* `ctrl+alt+b` => `clean/make`
* `ctrl+alt+shift+b` => `clean/make SGDK`
* `ctrl+shift+d` => `debug`
* `ctrl+alt+d` => `clean/make (debug)`
* `ctrl+alt+shift+d` => `clean/make SGDK debug`
* `F5` => `start debugging`

Example keybindings.json (File -> prefrences -> keybindings -> keybindings.json)

    [
        {
            "key": "ctrl+shift+b",
            "command": "workbench.action.tasks.runTask",
            "args": "make"
        },
        {
            "key": "ctrl+alt+b",
            "command": "workbench.action.tasks.runTask",
            "args": "clean/make"
        },
        {
            "key": "ctrl+alt+shift+b",
            "command": "workbench.action.tasks.runTask",
            "args": "clean/make SGDK"
        },
        {
            "key": "ctrl+shift+d",
            "command": "workbench.action.tasks.runTask",
            "args": "make (debug)"
        },
        {
            "key": "ctrl+alt+d",
            "command": "workbench.action.tasks.runTask",
            "args": "clean/make (debug)"
        },
        {
            "key": "ctrl+shift+alt+d",
            "command": "workbench.action.tasks.runTask",
            "args": "clean/make SGDK (debug)"
        },
    ]

Do the full `clean/make SGDK` version when switching between debugging and normal builds. Errors you likely will get if you forget:

    # attempted debug build with normal SGDK build (fix with `clean/build SGDK (debug)`)
    sgdk134/lib/libmd_debug.a: No such file or directory

    # attempted normal build with debug SGDK build (fix with `clean/build SGDK`)
    sgdk134/lib/libmd.a: No such file or directory

Disable optimizations (`-O` flag) in make files to not have `optimized_out` in the watch variables or windows (I used `-O0` in both `makefile.gen` and the SGDK `makelib.gen`). This project has a sample of both of these files as modified.

    # makefile.gen
    debug: FLAGS= $(DEFAULT_FLAGS) -O0 -ggdb -DDEBUG=1

    # makelib.gen
    debug: FLAGS_LIB= $(DEFAULT_FLAGS_LIB) -O0 -ggdb -DDEBUG=1

Turn on `use gdb` option in genskmod

Set a breakpoints, run program until one is triggerd. Screen will go blue when a breakpoint is hit (depending on settings).

> Sometimes things are a little finicky... (random exceptions being thrown, only with debug build)
>
> * stop debugger
> * exit genskmod
> * restart gensKmod (don't load rom yet)
> * start debugger (f5)
> * load rom
>
> Following these steps seems to have fixed mos of the issues I ran into