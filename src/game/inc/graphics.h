#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include <genesis.h>

#include "init.h"
#include "gfx.h"
#include "sprite.h"
#include "../models/creep.h"

#define MIN_POSX        FIX16(0)
#define MAX_POSX        FIX16(320-32)
#define MAX_POSY        FIX16(220)

// sprites structure (pointer of Sprite)
Sprite* sprites[3];

fix32 posx;
fix32 posy;

s16 xorder;
s16 yorder;

fix32 creepPosx[2];
fix32 creepPosy[2];
s16 creepXorder[2];

const SpriteDefinition sonic_sprite;
const SpriteDefinition creep_sprite;

void initGraphics(void);
void drawBackground(void);
void drawForeground(void);
void updatePhysic(void);
void updateTowers(void);
void updateAnim(void);

#endif