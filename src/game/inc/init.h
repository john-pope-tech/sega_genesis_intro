#ifndef _INIT_H_
#define _INIT_H_

#include "game/models/game.h"
#include "state.h"
#include "sound.h"

// global game object
Game game;

// global state function
StateFunc statefunc;

void initWave();

#endif