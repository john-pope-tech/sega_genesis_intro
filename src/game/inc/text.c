#include "text.h"

void drawTextCentered(char* str, s16 x, s16 y) {
    if(x < 0) {
        const u8 CHAR_PIXEL_WIDTH = 8;
        x = screenWidth / CHAR_PIXEL_WIDTH / 2;
    }

    if(y < 0) {
        const u8 CHAR_PIXEL_HEIGHT = 8;
        y = screenHeight / CHAR_PIXEL_HEIGHT / 2;
    }

    u16 lx = x - (u16)(strlen(str)/2);
    VDP_drawText(str, lx, y);
}

void drawTextRightJustified(char* str, s16 x, s16 y) {
    if(x <= 0) {
        const u8 CHAR_PIXEL_WIDTH = 8;
        x = screenWidth / CHAR_PIXEL_WIDTH + x;
    }

    if(y <= 0) {
        const u8 CHAR_PIXEL_HEIGHT = 8;
        y = screenHeight / CHAR_PIXEL_HEIGHT + y;
    }

    u16 lx = x - (u16)(strlen(str));
    VDP_drawText(str, lx, y);
}

void pointToStr(Point point, char* str, int decimals) {
    char tmp[256];
    char s[2] = "(";
    char sp[3]=", ";
    char e[2]=")";

    strcpy(str,s);
    fix16ToStr(point.x, tmp, decimals);
    strcat(str, tmp);
    strcat(str, sp);
    fix16ToStr(point.y, tmp, decimals);
    strcat(str, tmp);
    strcat(str, e);
}
