#include "input.h"

void myJoyHandler( u16 joy, u16 changed, u16 state)
{
	if (joy == JOY_1)
	{
		if (state & BUTTON_START)
		{
			//player 1 press START button
		}
		else if (changed & BUTTON_START)
		{
			//player 1 released START button
            game.isPlaying = !game.isPlaying;
		}

		if(state & BUTTON_LEFT) {
			game.player.direction = -1;
		} else if(state & BUTTON_RIGHT) {
			game.player.direction = 1;
		} else if(state & BUTTON_UP) {
			game.arena.focusedTower = (game.arena.focusedTower - 1) % game.arena.towerCount;
		} else if(state & BUTTON_DOWN) {
			game.arena.focusedTower = (game.arena.focusedTower + 1) % game.arena.towerCount;
		} else {
			game.player.direction = 0;
		}

		if (state & BUTTON_A) {
			game.player.isFiring = TRUE;
		}
	}
}
