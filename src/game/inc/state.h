#include <genesis.h>
#include "init.h"
#include "text.h"
#include "input.h"
#include "graphics.h"
#include "sounds.h"

void *initGame();
void *startScreen();
void *playGame();
void *endScreen();