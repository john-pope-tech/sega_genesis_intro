#include "graphics.h"

void initGraphics(){
    u16 palette[64];

    // disable interrupt when accessing VDP
    SYS_disableInts();
    // initialization
    VDP_setScreenWidth320();

    // init sprites engine
    SPR_init(80, 384*2, 256);

    // set all palette to black
    VDP_setPaletteColors(0, (u16*) palette_black, 64);

    // load background
    drawBackground();

    // VDP process done, we can re enable interrupts
    SYS_enableInts();

    posx = FIX32(48);
    posy = MAX_POSY;

    xorder = 0;
    yorder = 0;

    creepPosx[0] = FIX32(128);
    creepPosy[0] = FIX32(194);
    creepPosx[1] = FIX32(272);
    creepPosy[1] = FIX32(84);
    creepXorder[0] = 1;
    creepXorder[1] = -1;

    // prepare palettes
    memcpy(&palette[game.arena.background.palette * 16], game.arena.background.image->palette->data, 16 * 2);
    memcpy(&palette[game.arena.foreground.palette * 16], game.arena.foreground.image->palette->data, 16 * 2);
    memcpy(&palette[game.arena.tower.palette * 16], game.arena.tower.image->palette->data, 16 * 2);
    memcpy(&palette[48], creep_sprite.palette->data, 16 * 2);

    // fade in
    VDP_fadeIn(0, (4 * 16) - 1, palette, 20, FALSE);
}

void drawBackground() {
    u16 ind = TILE_USERINDEX;
    if(game.arena.background.image) {
        VDP_drawImageEx(PLAN_B, game.arena.background.image, TILE_ATTR_FULL(game.arena.background.palette, FALSE, FALSE, FALSE, ind), 0, 0, FALSE, TRUE);
        ind += game.arena.background.image->tileset->numTile;
    }
    if(game.arena.foreground.image) {
        VDP_drawImageEx(PLAN_A, game.arena.foreground.image, TILE_ATTR_FULL(game.arena.foreground.palette, FALSE, FALSE, FALSE, ind), 0, 0, FALSE, TRUE);
    }
}

void drawForeground() {
    if(game.arena.towerCount > 0) {
        u16 ind = TILE_USERINDEX;
        if(game.arena.background.image) { ind += game.arena.background.image->tileset->numTile; }
        if(game.arena.foreground.image) { ind += game.arena.foreground.image->tileset->numTile; }

        for(u8 i = 0;i<game.arena.towerCount;i++) {
            Point* towerLocation = &game.arena.towers[i].location;

            u8 tileScale = TILE_SIZE/4;
            u16 x = fix16ToInt(towerLocation->x) / tileScale;
            u16 y = fix16ToInt(towerLocation->y) / tileScale;

            u8 palette = game.arena.tower.palette;

            VDP_drawImageEx(PLAN_A, game.arena.tower.image, TILE_ATTR_FULL(palette, TRUE, FALSE, FALSE, ind), x, y, FALSE, TRUE);
            ind += game.arena.tower.image->tileset->numTile;
        }
    }
}

void updateTowers(){
    Creep* currentCreep;
    Tower* currentTower;

    // loop each creep
    for(u8 c=0;c<game.wave.creepCounters[CREEP_TOTAL];c++) {
        currentCreep = &game.wave.creep[c];

        if(currentCreep->isActive == TRUE) {
            // find towers that are in range
            for(u8 i=0;i<game.arena.towerCount;i++) {
                currentTower = &game.arena.towers[i];

                // TODO: make fully dynamic (can't always do it this way yet... sprite isn't initialized if over limit for screen, some game modes you may want the sprite to update even if it isn't rendered)
                u16 creepHeight = 32;
                u16 creepWidth = 32;

                if(currentCreep->graphic.sprite != NULL) {
                    creepHeight = currentCreep->graphic.sprite->frame->h;
                    creepWidth = currentCreep->graphic.sprite->frame->w;
                }                

                s32 dx = fix16ToRoundedInt(fix16Sub(currentCreep->location.x+FIX16(creepWidth/2), currentTower->location.x+FIX16(16)));
                s32 dy = fix16ToRoundedInt(fix16Sub(currentCreep->location.y+FIX16(creepHeight/2), currentTower->location.y+FIX16(16)));
                u32 distance = getApproximatedDistance(dx, dy);

                u8 inRange = distance < currentTower->range ? TRUE : FALSE;
                
                if(inRange == TRUE) {
                    currentCreep->life = currentCreep->life > currentTower->damage ? currentCreep->life - currentTower->damage : 0;

                    if(currentCreep->graphic.sprite != NULL) {
                        SPR_setPalette(currentCreep->graphic.sprite, PAL1);
                    }

                    if(currentCreep->life <= 0) {
                        // update player balance, make sure it stays in range
                        s16 balance = min(game.player.balance + currentCreep->bounty, game.player.maxBalance);
                        // don't go lower than 0
                        game.player.balance = max(balance, 0);

                        // creep is dead, no more towers need to shoot at it
                        break;
                    }
                }
            }
        }
    }
}

void updatePhysic(){
    u16 i;

    // creep physic
    if (game.player.direction > 0 && game.player.location.x < MAX_POSX) {
        game.player.location.x += FIX16(2.9);
    } else if(game.player.direction < 0 && game.player.location.x > MIN_POSX) {
        game.player.location.x -= FIX16(2.9);
    }

    if ((game.player.location.x >= MAX_POSX) || (game.player.location.x <= MIN_POSX)) {
        game.player.direction = -game.player.direction;
    }

    SPR_setPosition(sprites[1], fix16ToInt(game.player.location.x), fix16ToInt(game.player.location.y));
    SPR_setPosition(sprites[2], fix16ToInt(game.arena.towers[game.arena.focusedTower].location.x), fix16ToInt(game.arena.towers[game.arena.focusedTower].location.y));

    s16 radiusIndex = game.arena.towers[game.arena.focusedTower].range <=30 ? 2 : game.arena.towers[game.arena.focusedTower].range <= 40 ? 1 : 0;
    SPR_setAnim(sprites[3], radiusIndex);
    SPR_setPosition(sprites[3], fix16ToInt(game.arena.towers[game.arena.focusedTower].location.x)-64+16, fix16ToInt(game.arena.towers[game.arena.focusedTower].location.y)-64+16);

    updatePlayerBullet(&game.player);

    Creep* currentCreep;

    for(i=0; i<game.wave.creepCounters[CREEP_TOTAL]; i++) {
        currentCreep = &(game.wave.creep[i]);
        updateCreep(currentCreep, &game.arena.path, game.wave.creepCounters);
    }

    // no creep out or waiting, start next wave
    if(game.wave.creepCounters[CREEP_OUT] == 0 && game.wave.creepCounters[CREEP_WAITING] == 0) {
        // release skeleton (TODO: convert to game handler)
        SPR_releaseSprite(sprites[1]);
        initWave();
    }

    char wstr[8], str[48]="     ", sp[2]=" ", sep[2]="/", sepc[2]=":", e[10]="     ";
    u16 creepKilled = game.wave.creepCounters[CREEP_TOTAL] - game.wave.creepCounters[CREEP_OUT] - game.wave.creepCounters[CREEP_WAITING];

    if(creepKilled < game.wave.creepCounters[CREEP_TOTAL]) {
        if(game.wave.creepCounters[CREEP_WAITING] > 0) {
            uintToStr(game.wave.creepCounters[CREEP_OUT], wstr, 1);
            strcat(str, wstr);
            strcat(str, sepc);
        }

        u16 creepRemaining = game.wave.creepCounters[CREEP_OUT] + game.wave.creepCounters[CREEP_WAITING];
        uintToStr(creepRemaining, wstr, 1);
        strcat(str, wstr);

        strcat(str, sp);
    }

    uintToStr(creepKilled, wstr, 1);
    strcat(str, wstr);
    strcat(str, sep);
    uintToStr(game.wave.creepCounters[CREEP_TOTAL], wstr, 1);
    strcat(str, wstr);

    strcat(str, e);
    
    drawTextCentered(str, -1, 0);

    updateTowers();
}

void updateAnim(){
    
}
