#include "init.h"

Game game = {
    .isPlaying = 0,
    .state = init,
    .statefunc = initGame,
    .player = {
        .balance = 100,
        .maxBalance = 6666,
        .location = {
            .x = FIX16(128),
            .y = FIX16(194),
        },
        .speed = 200,
        .fireRate = 600,
        .bullet = {
            .location = {
                .x = 0,
                .y = 0,
            },
            .graphic = {
                .spriteDefinition = &bullet_sprite,
                .palette = PAL3,
            },
            .collisionGraphic = {
                .spriteDefinition = &fireball_sprite,
                .palette = PAL2,
            },
        },
    },
    .arena = {
        .towers = {
            { .location = {.x=FIX16(208),.y=FIX16(36)}, .range=30, .damage = 10 },
            { .location = {.x=FIX16(126),.y=FIX16(60)}, .range=40, .damage = 10 },
            { .location = {.x=FIX16(158),.y=FIX16(60)}, .range=60, .damage = 1 },
        },
        .towerCount = 3,
        .path = {
            .points = {
                {.x=FIX16(-40),.y=FIX16(10)},
                {.x=FIX16(95),.y=FIX16(10)},
                {.x=FIX16(95),.y=FIX16(50)},
                {.x=FIX16(50),.y=FIX16(50)},
                {.x=FIX16(50),.y=FIX16(25)},
                {.x=FIX16(178),.y=FIX16(25)},
                {.x=FIX16(178),.y=FIX16(100)},
                {.x=FIX16(225),.y=FIX16(100)},
                {.x=FIX16(225),.y=FIX16(125)},
                {.x=FIX16(315),.y=FIX16(125)},
                {.x=FIX16(315),.y=FIX16(165)},
                {.x=FIX16(0),.y=FIX16(165)},
                {.x=FIX16(0),.y=FIX16(42)},
                {.x=FIX16(-100),.y=FIX16(42)},
            },
            .length = 14,
        },
        .background = {
            .image = &bgb_image,
            .palette = 1
        },
        .foreground = {
            .image = &bga_image,
            .palette = 0
        },
        .tower = {
            .image = &arrow_tower_image,
            .palette = 2
        },
    },
    .wave = {
        .number = 26,
        .creepCounters = { 0, 0, 0 },
        .spawnRate = 300,
        .difficulty = {
            .lifeMultiplier = FIX16(.2),
        },
    },
};

void initWave() {
    // hardcoded skeleton
    sprites[1] = SPR_addSprite(&skeleton_sprite, fix16ToInt(game.player.location.x), fix16ToInt(game.player.location.y), TILE_ATTR(PAL3, TRUE, FALSE, FALSE));
    sprites[2] = SPR_addSprite(&selection_sprite, fix16ToInt(game.arena.towers[0].location.x), fix16ToInt(game.arena.towers[0].location.y), TILE_ATTR(PAL3, TRUE, FALSE, FALSE));
    sprites[3] = SPR_addSprite(&radius_sprite, fix16ToInt(game.arena.towers[0].location.x), fix16ToInt(game.arena.towers[0].location.y), TILE_ATTR(PAL3, TRUE, FALSE, FALSE));

    SPR_setAnim(sprites[1], 0);
    SPR_setAnim(sprites[2], 0);
    SPR_setAnim(sprites[3], 0);

    initBullet(&game.player.bullet);

    game.wave.number += 1;

    VDP_clearTextLine(1);
    char wstr[4], str[16] = "Wave ";
    uintToStr(game.wave.number, wstr, 1);
    strcat(str, wstr);
    VDP_drawText(str, 1, 1);

    // TODO: remove min(), use waiting queue instead
    game.wave.creepCounters[CREEP_TOTAL] = min((game.wave.number <= 3 ? 3 : game.wave.number < 6 ? 6 : game.wave.number < 9 ? 9 : 12) + game.wave.number, CREEP_MAX_COUNT);
    game.wave.creepCounters[CREEP_OUT] = 0;
    game.wave.creepCounters[CREEP_WAITING] = game.wave.creepCounters[CREEP_TOTAL];

    Creep* currentCreep;

    for(int i=0; i<game.wave.creepCounters[CREEP_TOTAL]; i++) {
        currentCreep = &game.wave.creep[i];
        currentCreep->bounty = 3;
        currentCreep->life = 500;
        currentCreep->position = 0;
        currentCreep->speed = FIX16(100);
        currentCreep->isActive = FALSE;
        currentCreep->lastUpdateTick = getTick() + (i * game.wave.spawnRate);

        currentCreep->graphic.spriteDefinition = &creep_sprite;
        currentCreep->graphic.palette = PAL3;
        
        currentCreep->location.x = game.arena.path.points[0].x;
        currentCreep->location.y = game.arena.path.points[0].y;

        currentCreep->graphic.sprite = NULL;
        initCreepSprite(currentCreep);
    }

    SPR_update();
}