#include "state.h"

void *initGame() {
    JOY_init();
    JOY_setEventHandler( &myJoyHandler );

    initGraphics();
    initSounds();

    game.state = start;
    
    return startScreen;
}

void *startScreen() {
    if(game.isPlaying == FALSE) {
        char str[16], str2[16];
        uintToStr(screenWidth, str, 1);
        uintToStr(screenHeight, str2, 1);
        strcat(str, "x");
        strcat(str, str2);
        drawTextCentered(str, -1, 1);

        drawTextCentered("Scope Creep", -1, 13);
	    drawTextCentered("<press start>", -1, 20);

        return startScreen;
    } else {
        game.state = playing;
        game.start = getTick();
        
        VDP_clearText(0, 13, 40);
	    VDP_clearText(0, 20, 40);

        drawForeground();

        return playGame;
    }
}

void *playGame() {
    if(game.isPlaying == TRUE) {
        u32 ticks = getTick();
        u32 elapsed = (ticks - game.start) / TICKPERSECOND;

        updatePhysic();
        updateAnim();

        SPR_update();
        
        // debug: count sprites active
        char str[32]="",sstr[16];
        
        uintToStr(elapsed, sstr, 1);
        strcat(str, sstr);
        drawTextCentered(str, -1, 27);

        strcpy(str, "      ");
        uintToStr(SPR_getNumActiveSprite(), sstr, 1);
        strcat(str, sstr);
        drawTextRightJustified(str, -1, -1);

        char wstr[16], bstr[16] = "      $";
        
        uintToStr(game.player.balance, wstr, 1);
        strcat(bstr, wstr);
        drawTextRightJustified(bstr, -1, 1);
        
        // uintToStr(ticks, str, 1);
        // VDP_drawText(str, 100, 40);

        return playGame;
    } else {
        game.state = end;
        return endScreen;
    }
}

void *endScreen() {
    if(game.isPlaying == FALSE) {
        drawTextCentered("Game Over", -1, 14);

        return endScreen;
    } else {
        // reset
        VDP_clearText(0,1,40);
        VDP_clearText(0,14,40);

        VDP_clearSprites();

        game.isPlaying = FALSE;
        game.state = start;
        return startScreen;
    }
}