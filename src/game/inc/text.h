#include <genesis.h>
#include "game/models/point.h"

void drawTextCentered(char* str, s16 x, s16 y);
void drawTextRightJustified(char* str, s16 x, s16 y);
void pointToStr(Point point, char* str, int decimals);