#ifndef _CREEP_H_
#define _CREEP_H_

#include <genesis.h>
#include "point.h"
#include "path.h"
#include "graphic.h"
#include "sounds.h"

typedef struct {
    s16 bounty;
    u16 life;
    u8 position;
    u8 isActive;
    fix16 speed;
    u32 lastUpdateTick;

    Point location;
    Graphic graphic;
} Creep;

#define CREEP_MAX_COUNT 100
#define CREEP_TOTAL 0
#define CREEP_OUT 1
#define CREEP_WAITING 2

s16 updateCreep(Creep* creep, Path* path, u8* waveCounters);
void initCreepSprite(Creep* creep);

#endif