#ifndef _PLAYER_H_
#define _PLAYER_H_

#include <genesis.h>
#include "graphic.h"
#include "point.h"
#include "bullet.h"

typedef struct {
    u8 life;
    u16 balance;
    u16 maxBalance;

    char name[50];

    Point location;

    s8 direction;
    u8 isFiring;
    u32 lastFireTick;
    fix16 speed;
    u16 fireRate;
    
    Bullet bullet;
} Player;

#include "game/inc/init.h"

void playerFire(Player*);
void updatePlayerBullet(Player*);
void checkPlayerBulletCollision(Player*);

#endif