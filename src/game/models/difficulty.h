#include <genesis.h>

typedef struct {
    fix16 lifeMultiplier;
    fix16 speedMultiplier;
    fix16 bountyMultiplier;
} Difficulty;