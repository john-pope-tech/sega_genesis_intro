#ifndef _BULLET_H_
#define _BULLET_H_

#include <genesis.h>
#include "point.h"
#include "graphic.h"

typedef struct {
    Point location;
    Graphic graphic;
    Graphic collisionGraphic;

    fix16 speed;
    u32 lastUpdateTick;
} Bullet;

void initBullet(Bullet*);

#endif