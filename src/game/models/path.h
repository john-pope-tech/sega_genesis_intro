#ifndef _PATH_H_
#define _PATH_H_

#include "point.h"

typedef struct {
    u8 length; 
    Point points[32];
} Path;

#endif