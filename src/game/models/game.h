#include <genesis.h>
#include "player.h"
#include "wave.h"
#include "arena.h"
#include "point.h"
#include "graphic.h"

typedef enum {init, start, playing, end} GameState;
typedef void *(*StateFunc)();

typedef struct {
    u8 isPlaying;
    Player player;
    GameState state;
    Wave wave;
    Arena arena;
    u32 start;
    StateFunc statefunc;
} Game;
