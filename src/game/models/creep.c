#include "creep.h"

s16 updateCreep(Creep* creep, Path* path, u8* waveCounters) {
    u32 tick = getTick();
    s32 timeDelta = tick - creep->lastUpdateTick;
    
    // creep is paused, don't move
    if(timeDelta < 0) {
        return 0;
    }

    // check if sprite hasn't been rendered yet, this will load it now if it hasn't
    initCreepSprite(creep);

    // creep can't render... too many sprites on screen, keep it paused
    if(creep->graphic.sprite == NULL) {
        creep->lastUpdateTick = tick + 100;
        return 0;
    }

    // creep is dead
    if(creep->life <= 0) {
        // release sprite, set flags, update counter
        if(creep->graphic.sprite != NULL) {
            SPR_releaseSprite(creep->graphic.sprite);
            creep->graphic.sprite = NULL;
        }

        if(creep->isActive == TRUE) {
            creep->isActive = FALSE;
            waveCounters[CREEP_OUT]--;

            if(waveCounters[CREEP_TOTAL] - waveCounters[CREEP_WAITING] - waveCounters[CREEP_OUT] == 26) {
                // play sf I got 26
                SND_startPlay_PCM(i_got_26_8k, sizeof(i_got_26_8k), SOUND_RATE_8000, SOUND_PAN_CENTER, FALSE);
            }

            return creep->bounty;
        }

        // dead, don't update this creep
        return 0;
    }

    if(creep->isActive == FALSE) {
        creep->isActive = TRUE;
        waveCounters[CREEP_WAITING]--;
        waveCounters[CREEP_OUT]++;
    }

    fix16 elapsed = fix16Div(intToFix16(timeDelta), intToFix16(TICKPERSECOND));
    fix16 stepDistance = fix16Mul(creep->speed, elapsed);
    Point* target = &path->points[creep->position];
    s8 dirX = 0;
    s8 dirY = 0;

    creep->lastUpdateTick = tick;

    // if(creep->graphic.spriteDefinition->palette->index != creep->graphic.palette) {
        SPR_setPalette(creep->graphic.sprite, creep->graphic.palette);
    // }

    u8 hFlip, animation;

    if(creep->location.x < target->x) {
        dirX = 1;
        hFlip = FALSE;
        animation = 2;
    } else if(creep->location.x > target->x) {
        dirX = -1;
        hFlip = TRUE;
        animation = 2;
    } else if(creep->location.y < target->y) {
        dirY = 1;
        hFlip = FALSE;
        animation = 0;
    } else if(creep->location.y > target->y) {
        dirY = -1;
        hFlip = FALSE;
        animation = 1;
    }

    // change happening, update animation
    if(dirX != 0 || dirY != 0) {
        if(creep->graphic.sprite) {
            SPR_setHFlip(creep->graphic.sprite, hFlip);
            SPR_setAnim(creep->graphic.sprite, animation);
        }
    }

    // if either direction is negative, need to invert stepDistance
    if(dirX < 0 || dirY  < 0) {
        stepDistance = fix16Neg(stepDistance);
    }
    
    if(dirX != 0) {
        fix16 offsetX = fix16Sub(target->x, creep->location.x);
        if(abs(fix16ToRoundedInt(offsetX)) < abs(fix16ToRoundedInt(stepDistance))) {
            creep->location.x = target->x;
        } else {
            creep->location.x = fix16Add(creep->location.x, stepDistance);
        }
    }

    if(dirY != 0) {
        fix16 offsetY = fix16Sub(target->y, creep->location.y);
        if(abs(fix16ToRoundedInt(offsetY)) < abs(fix16ToRoundedInt(stepDistance))) {
            creep->location.y = target->y;
        } else {
            creep->location.y = fix16Add(creep->location.y, stepDistance);
        }
    }

    if(creep->location.x == target->x && creep->location.y == target->y) {
        creep->position = (creep->position + 1) % path->length;
    }

    SPR_setPosition(creep->graphic.sprite, fix16ToInt(creep->location.x), fix16ToInt(creep->location.y));

    return 0;
}

void initCreepSprite(Creep* creep) {
    // sprite not already added?
    if(creep->graphic.sprite == NULL) {
        // add the sprite
        creep->graphic.sprite = SPR_addSprite(
            creep->graphic.spriteDefinition,
            fix16ToInt(creep->location.x),
            fix16ToInt(creep->location.y),
            TILE_ATTR(creep->graphic.palette,
                TRUE,
                FALSE,
                FALSE
            )
        );
        
        // set initial animation sequence
        if(creep->graphic.sprite != NULL) {
            SPR_setAnim(creep->graphic.sprite, 2);
        }
    }
}