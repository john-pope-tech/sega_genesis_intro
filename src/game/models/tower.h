#include <genesis.h>
#include "point.h"

typedef struct {
    char type[25];
    u8 cost;
    u8 speed;
    u8 damage;
    u8 range;

    Point location;
} Tower;
