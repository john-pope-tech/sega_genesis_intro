#include <genesis.h>
#include "path.h"
#include "tower.h"
#include "graphic.h"

typedef struct {
    Path path;
    Point foundations[25];
    Tower towers[25];

    u8 towerCount;
    u8 focusedTower;
    u8 foundationCount;

    Graphic background;
    Graphic foreground;
    Graphic tower;
} Arena;
