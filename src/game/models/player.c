#include "player.h"

void playerFire(Player* player) {
    u32 tick = getTick();

    // too soon since last time we fired
    if(player->lastFireTick + player->fireRate > tick) {
        return;
    }

    // get time delta since last fire, see if past rate, if so, start animation on bullet
    player->lastFireTick = getTick();
    SPR_setVisibility(player->bullet.graphic.sprite, VISIBLE);
    player->bullet.location.x = player->location.x + FIX16(11); // width of player graphic to center bullet on player
    player->bullet.location.y = player->location.y;
    player->bullet.lastUpdateTick = tick;
    player->bullet.speed = FIX16(200);
}

void updatePlayerBullet(Player* player) {
    if (player->isFiring == TRUE){
        playerFire(player);
    }

    u32 tick = getTick();
    s32 timeDelta = tick - player->bullet.lastUpdateTick;

    fix16 elapsed = fix16Div(intToFix16(timeDelta), intToFix16(TICKPERSECOND));

    if(player->isFiring == TRUE && player->bullet.location.y > 0) {
        u32 tick = getTick();
        s32 timeDelta = tick - player->bullet.lastUpdateTick;

        fix16 elapsed = fix16Div(intToFix16(timeDelta), intToFix16(TICKPERSECOND));
        fix16 stepDistance = fix16Mul(player->bullet.speed, elapsed);
        
        player->bullet.location.y -= stepDistance;

        checkPlayerBulletCollision(player);
    
        SPR_setPosition(player->bullet.graphic.sprite, fix16ToInt(player->bullet.location.x), fix16ToInt(player->bullet.location.y));
    } else {
        SPR_setVisibility(player->bullet.graphic.sprite, HIDDEN);
        
        player->isFiring = FALSE;

        if(elapsed > FIX16(2)) {
            SPR_setVisibility(player->bullet.collisionGraphic.sprite, HIDDEN);
        }
    }
}

void checkPlayerBulletCollision(Player* player) {
    int creepCount = game.wave.creepCounters[CREEP_TOTAL];

    for(int i=0;i<creepCount;i++) {
        Creep* currentCreep = &game.wave.creep[i];

        if(currentCreep->life <= 0 || player->bullet.speed == 0) {
            continue;
        }

        fix16 creepWidth = FIX16(32);
        fix16 creepHeight = FIX16(32);
        fix16 bulletWidth = FIX16(8);
        fix16 bulletHeight = FIX16(8);

        if(
            player->bullet.location.x >= currentCreep->location.x - bulletWidth/2
            && player->bullet.location.x <= currentCreep->location.x + creepWidth - bulletHeight/2
            && player->bullet.location.y >= currentCreep->location.y - bulletHeight/2
            && player->bullet.location.y <= currentCreep->location.y + creepHeight - bulletHeight/2
        ) {
            currentCreep->life = 0;
            currentCreep->position = 0;
            player->bullet.speed = 0;
            
            player->isFiring = FALSE;

            SPR_setPosition(player->bullet.collisionGraphic.sprite, fix16ToInt(player->bullet.location.x), fix16ToInt(player->bullet.location.y));
            SPR_setVisibility(player->bullet.collisionGraphic.sprite, VISIBLE);
        }
    }
}
