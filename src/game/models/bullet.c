#include "bullet.h"

void initBullet(Bullet* bullet) {
    bullet->location.x = 0;
    bullet->location.y = 0;
    bullet->speed = FIX16(200);

    if(bullet->graphic.sprite == NULL) {
        bullet->graphic.sprite = SPR_addSprite(
            bullet->graphic.spriteDefinition,
            bullet->location.x,
            bullet->location.y,
            TILE_ATTR(
                bullet->graphic.palette,
                TRUE,
                FALSE,
                FALSE
            )
        );

        SPR_setAnim(bullet->graphic.sprite, 0);
        // SPR_setVisibility(bullet->graphic.sprite, HIDDEN);
        SPR_setVisibility(bullet->graphic.sprite, VISIBLE);
    }

    if(bullet->collisionGraphic.sprite == NULL) {
        bullet->collisionGraphic.sprite = SPR_addSprite(
            bullet->collisionGraphic.spriteDefinition,
            bullet->location.x,
            bullet->location.y,
            TILE_ATTR(
                bullet->collisionGraphic.palette,
                TRUE,
                FALSE,
                FALSE
            )
        );

        SPR_setAnim(bullet->collisionGraphic.sprite, 0);
        SPR_setVisibility(bullet->collisionGraphic.sprite, HIDDEN);
    }
}
