#ifndef _POINT_H_
#define _POINT_H_

#include <genesis.h>

typedef struct {
    fix16 x;
    fix16 y;
    u8 isEmpty;
} Point;

#endif