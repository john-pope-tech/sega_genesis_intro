#ifndef _GRAPHIC_H_
#define _GRAPHIC_H_

#include <genesis.h>

typedef struct {
    const Image* image;
    const SpriteDefinition* spriteDefinition;
    Sprite* sprite;
    u8 palette;
} Graphic;

#endif