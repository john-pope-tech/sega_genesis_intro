#ifndef _WAVE_H_
#define _WAVE_H_

#include <genesis.h>
#include "creep.h"
#include "difficulty.h"

typedef struct {
    u8 isActive;
    u8 number;

    u8 creepCounters[3];
    
    u32 spawnRate;

    Creep creep[CREEP_MAX_COUNT];
    Difficulty difficulty;
} Wave;

#endif